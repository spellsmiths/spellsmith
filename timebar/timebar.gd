extends Node2D

export (float , 0, 1) var value = 0 setget set_value

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if delta >= 0.0:
		$Timer_bar.points[1].x = value * 100
		$Timer_bar/Label.text = String(value)
	
func set_value(val: float):
	value = val
