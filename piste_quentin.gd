extends Node2D

var delta_sum_ := 0.0
onready var allNotes := {
	60: {
		"color": Color.purple,
		"key": "left",
		"node": get_node("buttons/left"),
		"explosionPlayer": get_node("explosion1/AnimationPlayer"),
		"queue": []
	},
	61: {
		"color": Color.aqua,
		"key": "up",
		"node": get_node("buttons/up"),
		"explosionPlayer": get_node("explosion2/AnimationPlayer"),
		"queue": []
	},
	62: {
		"color": Color.blue,
		"key": "down",
		"node": get_node("buttons/down"),
		"explosionPlayer": get_node("explosion3/AnimationPlayer"),
		"queue": []
	},
	63: {
		"color": Color.red,
		"key": "right",
		"node": get_node("buttons/right"),
		"explosionPlayer": get_node("explosion4/AnimationPlayer"),
		"queue": []
	}
}

func _ready():
	for s in allNotes.values():
		s.node.color = s.color

func _process(delta):
	delta_sum_ += delta
	
	for s in allNotes.values():
		if Input.is_action_just_pressed(s.key):
			if not s.queue.empty():
				if s.queue.front().test_hit(delta_sum_):
					s.queue.pop_front().hit(s.node.global_position)
					print("hit")
				else:
					print("TOO EARLY")
			else:
				print("WUT??")
				
		if not s.queue.empty():
			if s.queue.front().test_miss(delta_sum_):
				s.queue.pop_front().miss()
				s.explosionPlayer.play("Explode")
				print("miss");

	for s in allNotes.values():
		s.node.pressed = Input.is_action_pressed(s.key)
	
	if delta_sum_ >= 1.0 and not $music.playing:
		$music.play()

func _on_midi_event(channel, event):
	if channel.track_name == "animation":
		
		var s = allNotes.get(event.note)
		
		if s and event.type == 1:
			var i = preload("res://tutorial/note.tscn").instance()
			add_child(i)
			i.expected_time     = delta_sum_ + 1.0
			i.global_rotation   = s.node.global_rotation
			i.global_position.y = s.node.global_position.y - 500
			i.global_position.x = s.node.global_position.x
			i.color             = s.color
			s.queue.push_back(i)
