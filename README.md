# Godot MIDI Player

Software MIDI player library for Godot Engine 3.2 later

* Changes play speed.
* Set tempo.
* Emit on some events (tempo change, appears lyric ...)
* Can ontrol like AudioStreamPlayer.

## Try it

1. Copy *.mid under "res://"
2. Copy *.sf2 under "res://"
3. Set MIDI path to MidiPlayer "file" parameter.
4. Set SoundFont path to MidiPlayer "soundfont" parameter.
5. Play

## How to use

* See [wiki](https://bitbucket.org/arlez80/godot-midi-player/wiki/)

### Demo

* [download](https://bitbucket.org/arlez80/godot-midi-player/downloads/demo.zip)
    * This demo can get MIDIInput events. You can play using MIDI keyboards!
* BGM "failyland_gm.mid" from [IvyMaze]( http://ivymaze.sakura.ne.jp/ )

## Hint

* Set false to `GodotMIDIPlayer.load_all_voices_from_soundfont` to load voices for program change message in MIDI sequence.
    * of course, `GodotMIDIPlayer.load_all_voices_from_soundfont = true` will be very slow.
* SMF format 0 loading faster than SMF format 1.
    * because format 1 data will be convert to format 0 in the player.

## TODO

* See [issues]( https://bitbucket.org/arlez80/godot-midi-player/issues )

## Not TODO

* Supports play format 2
    * SMF.gd can read it. but I will not implement it to MIDI Player.

## License

MIT License

spells animation :

https://v-ktor.itch.io/spell-animations
Creative Commons Attribution v4.0 International

water spell effect 01
https://pimen.itch.io/magical-water-effect
CC-BY

Water Spell Effect 02
https://pimen.itch.io/water-spell-effect-02
CC-BY

Fire spell Effect 02
https://pimen.itch.io/fire-spell-effect-02
CC-BY

Earth Spell Effect 02
https://pimen.itch.io/earth-spell-effect-2
CC-BY


Earth Spell Effect 01
https://pimen.itch.io/earth-spell-effect-01
CC-BY

characters :

wizard
https://zys37.itch.io/wizard-fantasy-character-spritesheet
CC-by

witch
https://www.deviantart.com/getanimated/art/Kiki-s-DS-498351260
CC-by-SA

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
