extends Node

export (bool) var autostart:bool = true
export (float) var time_scale_factor:float = 1

var current_index: int = 0

onready var Interval: Timer = $Interval
onready var SoundPlayers: Array = [$bing1, $bing2, $bing3, $bing4]

onready var interval_time: float = Interval.wait_time
onready var total_loop_time: float = interval_time * SoundPlayers.size()

func set_time_scale_factor(val: float):
	Interval.wait_time = interval_time * val

func _ready():
	if autostart:
		start()

func start():
	set_time_scale_factor(time_scale_factor)
	Interval.start()
	current_index = 0

func stop():
	Interval.stop()


func _on_Interval_timeout():
	SoundPlayers[current_index].play()
	current_index = (current_index + 1) % SoundPlayers.size()
