class_name Order extends Container

export(String) var spellName setget set_spell_name, get_spell_name
export(String) var filePath
export(Dictionary) var noteMapping
export(int) var difficulty
export(int) var reward 

func set_spell_name(val: String):
	$spellName.text = val

func get_spell_name() -> String:
	return $spellName.text

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
