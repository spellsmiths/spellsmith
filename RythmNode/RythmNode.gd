extends Node2D
class_name RythmNode

signal success_perc_update( value )
signal music_finished( score, missedNotes )
signal failed_note
signal success_note
signal health_update( value )


export (String, FILE, "*.mid") var file:String = "" setget set_file
export (bool) var playing:bool = false
export (int, 0, 127) var waterKeyNote:int = 0 setget set_note_key_water
export (int, 0, 127) var fireKeyNote:int = 1 setget set_note_key_fire
export (int, 0, 127) var airKeyNote:int = 2 setget set_note_key_air
export (int, 0, 127) var earthKeyNote:int = 3 setget set_note_key_earth
export (String, FILE, "*.sf2") var soundfont:String = "" setget set_soundfont
export (float, 0.5, 5) var rythmSpeed: float = 1

const KEY_WATER_NAME = "key_water"
const KEY_FIRE_NAME = "key_fire"
const KEY_AIR_NAME = "key_air"
const KEY_EARTH_NAME = "key_earth"

const waterNotePath = "res://RythmNode/waterNote.tscn"
const fireNotePath = "res://RythmNode/fireNote.tscn"
const airNotePath = "res://RythmNode/airNote.tscn"
const earthNotePath = "res://RythmNode/earthNote.tscn"
const indicatorBarPath = "res://RythmNode/indicatorBar.tscn"


onready var nodeKeys := {
	waterKeyNote:{
		"node": get_node("buttons/waterdrop"),
		"explosionPlayer": get_node("explosion1/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/waterNote.tscn"),
		"queue": [],
		"key": KEY_WATER_NAME
	},
	fireKeyNote:{
		"node": get_node("buttons/fireball"),
		"explosionPlayer": get_node("explosion2/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/fireNote.tscn"),
		"queue": [],
		"key": KEY_FIRE_NAME
	},
	airKeyNote:{
		"node": get_node("buttons/air"),
		"explosionPlayer": get_node("explosion3/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/airNote.tscn"),
		"queue": [],
		"key": KEY_AIR_NAME
	},
	earthKeyNote:{
		"node": get_node("buttons/earth"),
		"explosionPlayer": get_node("explosion4/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/earthNote.tscn"),
		"queue": [],
		"key": KEY_EARTH_NAME
	}
}

# Declare member variables here.
var delta_sum_ := 0.0
var validNotes = 0
var missedNotes = 0
var totalNotes = 0
var health = 100
var music_finished = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	if playing:
		start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	delta_sum_ += delta
	
	var unvalidPressedNotes = 0
	for nodeKey in nodeKeys.values():
		if Input.is_action_just_pressed(nodeKey.key):
			play_touch_sound(nodeKey.key)
			totalNotes += 1
			if not nodeKey.queue.empty():
				if nodeKey.queue.front().test_hit(delta_sum_):
					nodeKey.queue.pop_front().hit(nodeKey.node.global_position)
					validNotes += 1
					nodeKey.explosionPlayer.play("Explode")
					print("hit")
					self.emit_signal("success_note")
				else:
					unvalidPressedNotes += 1
					print("TOO EARLY")
					self.emit_signal("failed_note")
			else:
				unvalidPressedNotes += 1
				print("WUT??")
				self.emit_signal("failed_note")
		if not nodeKey.queue.empty():
			if nodeKey.queue.front().test_miss(delta_sum_):
				nodeKey.queue.pop_front().miss()
				unvalidPressedNotes += 1
				print("miss");
				self.emit_signal("failed_note")
				totalNotes += 1
		self.emit_signal("success_perc_update", calcScore())
		
	for s in nodeKeys.values():
		s.node.pressed = Input.is_action_pressed(s.key)
	
	self.emit_signal("health_update", updateHealth(unvalidPressedNotes))
	
	if music_finished && delta_sum_ > music_finished + 1.500:
		print("c fini")
		stop()
		music_finished = 0
		self.emit_signal("music_finished", calcScore(), totalNotes - validNotes)

func updateHealth(unvalidPressedNotes: int) -> float:
	health = health - unvalidPressedNotes*5
	return health

func calcScore() -> float:
	return 0 if totalNotes == 0 else float(validNotes)/float(totalNotes)

func stop():
	print("Stop rythm node")
	$MidiMusicPlayer.stop()
	$MidiAnimationsEvent.stop()
	$AnvilClanking.stop()
	playing = false
	$sorcerer.stop()
	$witch.stop()

func start():
	print("Start rythm node")
	music_finished = 0
	health = 100
	self.emit_signal("health_update", health)	
	$MidiMusicPlayer.file = file
	$MidiAnimationsEvent.file = file
	$MidiMusicPlayer.play_speed = rythmSpeed
	$MidiAnimationsEvent.play_speed = rythmSpeed
	$AnvilClanking.set_time_scale_factor(60*2/(110*rythmSpeed))
	$MidiMusicPlayer.set_soundfont(soundfont)
	$MidiMusicPlayer.play(0)
	$MidiAnimationsEvent.play(900 * rythmSpeed)
	$sorcerer.play()
	$witch.play()
	$AnvilClanking.start()

func play_touch_sound(key: String):
	if key == KEY_WATER_NAME:
		$keyUpMusic.play()
	elif key == KEY_FIRE_NAME:
		$keyDownMusic.play()
	elif key == KEY_AIR_NAME:
		$keyRightMusic.play()
	elif key == KEY_EARTH_NAME:
		$keyLeftMusic.play()

func set_file( path:String ):
	file = path

func set_note_key_water(note: int):
	if nodeKeys == null:
		waterKeyNote = note
		return
	nodeKeys[note] = {
		"node": get_node("buttons/waterdrop"),
		"explosionPlayer": get_node("explosion1/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/waterNote.tscn"),
		"queue": [],
		"key": KEY_WATER_NAME
	}

func set_note_key_fire(note: int):
	fireKeyNote = note
	if nodeKeys == null:
		return
	nodeKeys[note] = {
		"node": get_node("buttons/fireball"),
		"explosionPlayer": get_node("explosion2/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/fireNote.tscn"),
		"queue": [],
		"key": KEY_FIRE_NAME
	}

func set_note_key_air(note: int):
	airKeyNote = note
	if nodeKeys == null:
		return
	nodeKeys[note] = {
		"node": get_node("buttons/air"),
		"explosionPlayer": get_node("explosion3/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/airNote.tscn"),
		"queue": [],
		"key": KEY_AIR_NAME
	}

func set_note_key_earth(note: int):
	earthKeyNote = note
	if nodeKeys == null:
		return
	nodeKeys[note] = {
		"node": get_node("buttons/earth"),
		"explosionPlayer": get_node("explosion4/AnimationPlayer"),
		"associatedNotenode" : preload("res://RythmNode/earthNote.tscn"),
		"queue": [],
		"key": KEY_EARTH_NAME
	}

func set_soundfont(path: String):
	soundfont = path

func start_new_music(path: String, keyWater: int, keyFire: int, keyAir: int, keyEarth: int):
	# Stop previous music
	stop()
	# Reset success count
	validNotes = 0
	totalNotes = 0
	
	nodeKeys.clear()
	
	# Set keys
	set_note_key_water(keyWater)
	set_note_key_fire(keyFire)
	set_note_key_air(keyAir)
	set_note_key_earth(keyEarth)
	set_file(path)
	
	# Start new music
	start()

var keys = {}

func _on_MidiAnimationsEvent_midi_event(channel, event):
	pass # Replace with function body.
	
	if event.type == 1 or event.type == 144: # or event.type == 128
		if ! keys.has(event.note):
			keys[event.note] = 1
		else:
			keys[event.note] = keys[event.note] +1
			
		var s = nodeKeys.get(event.note)
		if s:
			var i = s.associatedNotenode.instance()
			add_child(i)
			i.expected_time     = delta_sum_ + 1.0 / rythmSpeed
			i.rythmSpeed = rythmSpeed
			i.global_position.y = s.node.global_position.y - 500
			i.global_position.x = s.node.global_position.x
			s.queue.push_back(i)
			

func _on_MidiAnimationsEvent_finished():
	print("end music", calcScore())
	music_finished = delta_sum_
	pass # Replace with function body.


func _on_MidiMusicPlayer_changed_tempo(tempo):
	print(tempo)
	#$AnvilClanking.set_time_scale_factor(60*2/(tempo*rythmSpeed))
