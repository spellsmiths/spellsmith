tool
extends Sprite

export(bool)  var pressed setget set_pressed
export(Color) var color

func set_pressed(value:bool) -> void:
	pressed = value

func _ready():
	pass

func _process(delta:float) -> void:
	if pressed:
		modulate = lerp(modulate, color, 1.0)
		scale.y  = lerp(scale.y, 5, 1)
		scale.x  = lerp(scale.x, 5, 1)
	else:
		modulate = lerp(modulate, Color.gray, delta * 10)
		scale.y  = lerp(scale.y, 5, delta * 30)
		scale.x  = lerp(scale.x, 5, delta * 30)
