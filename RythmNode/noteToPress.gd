extends Sprite

export(float)  var expected_time
export(float) var rythmSpeed = 1

var state_ := ""

func test_hit(time:float) -> bool:
	if abs(expected_time - time) < get_tolereance():
		return true
	return false

func test_miss(time:float) -> bool:
	if time > expected_time + get_tolereance():
		return true
	return false

func hit(position_to_freeze:Vector2) -> void:
	state_ = "hit"
	global_position = position_to_freeze
	
func miss() -> void:
	state_ = "miss"

func _process(delta):
	if state_ == "hit":
		queue_free()
		return

	global_position.y += delta * 500.0 * rythmSpeed
		
	if state_ == "miss":
		if global_position.y > 600.0:
			queue_free()

func get_tolereance() -> float:
	return 0.2 if rythmSpeed >=1 else 0.2 / rythmSpeed
