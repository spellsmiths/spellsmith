extends Node2D

var delta_sum_ := 0.0

var left := []

var currentSound
var validateNotes = 0
var totalNotes = 0
var health = 100

onready var nodeKeys := {
	60: {
		"color": Color.purple,
		"node": get_node("buttons/left"),
	},
	61: {
		"color": Color.aqua,
		"node": get_node("buttons/up"),
	},
	62: {
		"color": Color.blue,
		"node": get_node("buttons/down"),
	},
	63: {
		"color": Color.red,
		"node": get_node("buttons/right"),
	}
}

onready var stuff := {
	60: {
		"color": Color.purple,
		"key": "left",
		"node": get_node("buttons/left"),
		"queue": []
	},
	61: {
		"color": Color.aqua,
		"key": "up",
		"node": get_node("buttons/up"),
		"queue": []
	},
	62: {
		"color": Color.blue,
		"key": "down",
		"node": get_node("buttons/down"),
		"queue": []
	},
	63: {
		"color": Color.red,
		"key": "right",
		"node": get_node("buttons/right"),
		"queue": []
	}
}


onready var sound_12bar1 := {
	"midi_file" : "12bar1.mid",
	"sound_file" : "12bar1.wav",
	"keys": {
		49: {
			"color": Color.aqua,
			"key": "up",
			"node": get_node("buttons/up"),
			"queue": []
		},
		52: {
			"color": Color.purple,
			"key": "left",
			"node": get_node("buttons/left"),
			"queue": []
		},
		38: {
			"color": Color.red,
			"key": "right",
			"node": get_node("buttons/right"),
			"queue": []
		}
	}
}

func _ready() -> void:
	$MidiMusicPlayer.set_volume_db(0)
	play_new_music(sound_12bar1)

func _process(delta):
	delta_sum_ += delta
	
	for s in currentSound.keys.values():
		if Input.is_action_just_pressed(s.key):
			totalNotes+=1
			print(s)
			play_touch_sound(s.key)
			if not s.queue.empty():
				if s.queue.front().test_hit(delta_sum_):
					s.queue.pop_front().hit(s.node.global_position)
					print("hit")
					validateNotes +=1
				else:
					print("TOO EARLY")
			else:
				print("WUT??")
				
		if not s.queue.empty():
			if s.queue.front().test_miss(delta_sum_):
				s.queue.pop_front().miss()
				print("miss")
				totalNotes+=1

	for s in currentSound.keys.values():
		s.node.pressed = Input.is_action_pressed(s.key)
		
	print("notes : ", validateNotes, "/", totalNotes)
	if totalNotes > 0:
		print("score: ", float(validateNotes) / float(totalNotes))
		
func play_new_music(sound):
	currentSound = sound
	validateNotes = 0
	totalNotes = 0
	$MidiMusicPlayer.stop()
	$MidiAnimationsEvent.stop()
	$MidiMusicPlayer.file = "res://assets/music_animations/"+sound.midi_file
	$MidiAnimationsEvent.file = "res://assets/music_animations/"+sound.midi_file
	$MidiMusicPlayer.play(0)
	$MidiAnimationsEvent.play(1000)
	
	for s in nodeKeys.values():
		s.node.color = s.color
	

func play_touch_sound(key):
	if key == 'up':
		$keyUpMusic.play()
	elif key == 'down':
		$keyDownMusic.play()
	elif key == 'right':
		$keyRightMusic.play()
	elif key == 'left':
		$keyLeftMusic.play()
	

func _on_midi_event(channel, event):
	if event.type == 1:
		var s = currentSound.keys.get(event.note)
		
		if s and event.type == 1:
			var i = preload("res://tutorial/note.tscn").instance()
			add_child(i)
			i.expected_time     = delta_sum_ + 1.0
			i.global_rotation   = s.node.global_rotation
			i.global_position.y = -400
			i.global_position.x = s.node.global_position.x
			i.color             = s.color
			s.queue.push_back(i)
