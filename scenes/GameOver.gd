extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var delta_sum_ = 0

const POSSIBLE_START_KEYS = ["start_game", "key_fire", "key_water", "key_air", "key_earth"]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	delta_sum_ += delta
	if delta_sum_ > 2.0:
		for possible_key in POSSIBLE_START_KEYS:
			if Input.is_action_just_pressed(possible_key):
				get_tree().change_scene("res://scenes/LaunchMenu.tscn")
