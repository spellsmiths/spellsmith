extends Node2D

var spellNameKey = "spellName"
var difficultyKey = "difficulty"
var rewardKey = "reward"
var noteMappingKey = "noteMapping"
var filePathKey = "filePath"
onready var timer = $Timer

var exampleOrder1 = {
	spellNameKey: "example spell",
	difficultyKey: 1,
	rewardKey: 1,
	noteMappingKey: {
		"upKeyNote": 38,
		"downKeyNote": 40,
		"leftKeyNote": 44,	
		"rightKeyNote": 52
	},
	filePathKey: "res://assets/music_animations/dance49.mid"
}

var rythmSpeed = 1.0
var orderPool: Array = []

func prepareAndAddOrder(orderData: Dictionary):
	var o: Order = preload("res://order.tscn").instance()
	o.spellName = orderData[spellNameKey]
	o.difficulty = orderData[difficultyKey]
	o.reward = orderData[rewardKey]
	o.noteMapping = orderData[noteMappingKey]
	o.filePath = orderData[filePathKey]
	$ordersContainer.add_child(o)
	

func _ready():
	fillOrderPool()
	prepareAndAddOrder(orderPool[0])
	prepareAndAddOrder(orderPool[1])
	prepareAndAddOrder(orderPool[2])
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_RythmNode_success_perc_update(value):
	pass # Replace with function body.


func _on_RythmNode_music_finished( score, missedNotes ):
	print("change music")
	if ( score > 0.7):
		print("Success")
		$Potion.visible = true
		timer.wait_time = 2.0
		timer.connect("timeout", self, "_on_potion_timer_timeout")
		timer.one_shot = true
		timer.start()
		rythmSpeed += 0.02
		$RythmNode.rythmSpeed = rythmSpeed
		$RythmNode.start_new_music("res://assets/music_animations/dance49.mid", 38, 40, 44, 52)
	else:
		print("Defeat")
		$RythmNode.stop()
		
func _on_potion_timer_timeout() -> void:
	$Potion.visible = false

func _on_RythmNode_health_update(value):
	$Control/HealthBar.set_value(value)
	if ($Control/HealthBar.get_value() == 0):
		print("No more life, defeat")
		$Potion.visible = true
		
		timer.connect("timeout", self, "_on_potion_timer_timeout")
		timer.one_shot = true
		timer.start()
		$RythmNode.stop()
		get_tree().change_scene("res://scenes/GameOver.tscn")

func _input(ev):
	if ev is InputEventMouseButton and ev.pressed and ev.button_index == BUTTON_LEFT:
		for o in $ordersContainer.get_children():
			var bugfixPosition = Vector2(ev.position.x, ev.position.y - 32)
			if o.get_rect().has_point(bugfixPosition):
				print("found in ", o.spellName)
				var newOrder = drawNewOrderFromPool()
				$ordersContainer.remove_child(o)
				prepareAndAddOrder(newOrder)
				$RythmNode.start_new_music(o.filePath, o.noteMapping["upKeyNote"], o.noteMapping["downKeyNote"], o.noteMapping["leftKeyNote"], o.noteMapping["rightKeyNote"])
				pass
			else:
				print("not found in ", o.spellName)
	
func drawNewOrderFromPool() -> Dictionary:
	var idx = randi() % orderPool.size()
	var newOrder = orderPool[idx]
	
	var isNew = false
	while !isNew:
		var foundInContainer = false
		for o in $ordersContainer.get_children():
			if o.spellName == newOrder[spellNameKey]:
				foundInContainer = true
				idx = (idx + 1) % orderPool.size()
				newOrder = orderPool[idx]
				break
		if !foundInContainer:
			isNew = true	
		
	return newOrder
	
func fillOrderPool():
	orderPool.append({
		spellNameKey: "Enhanced jumping",
		difficultyKey: 1,
		rewardKey: 1,
		noteMappingKey: {
			"upKeyNote": 38,
			"downKeyNote": 52,
			"leftKeyNote": 49,	
			"rightKeyNote": 1,
		},
		filePathKey: "res://assets/music_animations/12bar1.mid"
	})
	orderPool.append({
		spellNameKey: "Dispel Hangover",
		difficultyKey: 1,
		rewardKey: 1,
		noteMappingKey: {
			"upKeyNote": 69,
			"downKeyNote": 70,
			"leftKeyNote": 77,	
			"rightKeyNote": 88
		},
		filePathKey: "res://assets/music_animations/sumrscum.mid"
	})
	orderPool.append({
		spellNameKey: "Spellflix and chill",
		difficultyKey: 1,
		rewardKey: 1,
		noteMappingKey: {
			"upKeyNote": 52,
			"downKeyNote": 62,
			"leftKeyNote": 69,	
			"rightKeyNote": 71
		},
		filePathKey: "res://assets/music_animations/trotto.mid"
	})
	orderPool.append({
		spellNameKey: "Magic lullaby",
		difficultyKey: 1,
		rewardKey: 1,
		noteMappingKey: {
			"upKeyNote": 71,
			"downKeyNote": 72,
			"leftKeyNote": 69,	
			"rightKeyNote": 83
		},
		filePathKey: "res://assets/music_animations/ravens.mid"
	})
